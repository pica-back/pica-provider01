/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kallsony.ms.provider.proveedor01.models.externals;
/**
 *
 * @author Jorge
 */
public class RequestCreateOrderProvider {
    int Id;
    String date_shipped;
    String payment_status;
    float total;
    int items_total;
    Product products;
    

    public RequestCreateOrderProvider() {
        products = new Product();
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getDate_shipped() {
        return date_shipped;
    }

    public void setDate_shipped(String date_shipped) {
        this.date_shipped = date_shipped;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getItems_total() {
        return items_total;
    }

    public void setItems_total(int items_total) {
        this.items_total = items_total;
    }

    public Product getProducts() {
        return products;
    }

    public void setProducts(Product products) {
        this.products = products;
    }
    
    
    
    
}
