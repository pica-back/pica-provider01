/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kallsony.ms.provider.proveedor01.models.externals;

/**
 *
 * @author Jorge
 */
public class Data {
    int id;
    String name;
    String price;
    String brand_name;
    int inventory_level;
    String condition; //resource
    CustomURL custom_url;
public Data() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }
    
    public String getURL(){
        return custom_url.getUrl();
    }

    public void setCustom_url(CustomURL custom_url) {
        this.custom_url = custom_url;
    }

    
    public String getName() {
        return name;
    }

    public int getInventory_level() {
        return inventory_level;
    }

    public void setInventory_level(int inventory_level) {
        this.inventory_level = inventory_level;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }
    
    
    
}
