/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kallsony.ms.provider.proveedor01.service;

import com.kallsony.ms.provider.proveedor01.mapper.MapperListProductsToBusisnessObject;
import com.kallsony.ms.provider.proveedor01.mapper.MapperRequestOrderToProvider;
import com.kallsony.ms.provider.proveedor01.models.Product;
import com.kallsony.ms.provider.proveedor01.models.RequestCreateOrderKs;
import com.kallsony.ms.provider.proveedor01.models.ResponseCreateOrderKs;
import com.kallsony.ms.provider.proveedor01.models.ResponseListProducts;
import com.kallsony.ms.provider.proveedor01.models.externals.RequestCreateOrderProvider;
import com.kallsony.ms.provider.proveedor01.models.externals.ResponseListProductsProvider;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Jorge
 */
@Service
public class ProviderServiceImpl implements ProviderService {

    @Override
    public ResponseListProducts getAllProducts() {
        // hacer mapeo request

        //hacer invocacion
        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
                @Override
                public boolean hasError(HttpStatus statusCode) {
                    return false;
                }
            });

            ResponseEntity<ResponseListProductsProvider> rs;
            rs = restTemplate.getForEntity("https://1c446216-1420-45ae-85d0-5e8fcdeff417.mock.pstmn.io", ResponseListProductsProvider.class);
            int code = rs.getStatusCodeValue();
            //mapeo response
            List<Product> products;
            products = MapperListProductsToBusisnessObject.MapOBject(rs.getBody());
            ResponseListProducts responseServide = new ResponseListProducts();
            responseServide.setStatusCode("0");
            responseServide.setProducts(products);
            //retornar respuesta
            return responseServide;

        } catch (Exception e) {
            ResponseListProducts responseServide = new ResponseListProducts();
            responseServide.setStatusCode("1");
            return responseServide;
        }
    }

    @Override
    public ResponseCreateOrderKs createOrder(RequestCreateOrderKs order) {
        // hacer mapeo request
        MapperRequestOrderToProvider op = new MapperRequestOrderToProvider();
        RequestCreateOrderProvider rq = op.MapperOrderRequest(order);
        //hacer invocacion
        /*try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
                @Override
                public boolean hasError(HttpStatus statusCode) {
                    return false;
                }
            });
        ResponseEntity<ResponseCreateOrderKs> rs;
            rs = restTemplate.getForEntity("https://a8bcc0c37-2c6e-49e0-84b3-fb10a320d3be.mock.pstmn.io/products", ResponseCreateOrderKs.class);
            
        //mapeo response
        int code = rs.getStatusCodeValue();
        if (code == 200){
            
        }else{
            
            ResponseCreateOrderKs errorRs = new ResponseCreateOrderKs();
            errorRs.setStatusCode(500);
            return errorRs;
        }
        
        //retornar respuesta
        return null;
        
        } catch (Exception e) {
            ResponseListProducts responseServide = new ResponseListProducts();
            responseServide.setStatusCode("1");
            ResponseCreateOrderKs errorRs = new ResponseCreateOrderKs();
            errorRs.setStatusCode(500);
            
            return errorRs;
        }*/
        ResponseCreateOrderKs rs = new ResponseCreateOrderKs();
        rs.setMessage("213431232");
        rs.setStatusCode(0);
        return rs;
    }

}
