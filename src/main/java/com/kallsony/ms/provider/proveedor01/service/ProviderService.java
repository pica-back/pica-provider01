/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kallsony.ms.provider.proveedor01.service;

/**
 *
 * @author Jorge
 */

import com.kallsony.ms.provider.proveedor01.models.RequestCreateOrderKs;
import com.kallsony.ms.provider.proveedor01.models.ResponseCreateOrderKs;
import com.kallsony.ms.provider.proveedor01.models.ResponseListProducts;



public interface ProviderService {

    ResponseListProducts  getAllProducts();
    
    ResponseCreateOrderKs createOrder(RequestCreateOrderKs order);
}
