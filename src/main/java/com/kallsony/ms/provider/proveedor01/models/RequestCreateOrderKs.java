/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kallsony.ms.provider.proveedor01.models;

import java.util.List;

/**
 *
 * @author Jorge
 */
public class RequestCreateOrderKs {
    String email;
    String direccion;
    float valor;
    int cantidad;
    String idproductprovider;
    String nomproducto;
    String proveedor;
    String estado;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public float getVslor() {
        return valor;
    }

    public void setVslor(float vslor) {
        this.valor = vslor;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getIdproductprovider() {
        return idproductprovider;
    }

    public void setIdproductprovider(String idproductprovider) {
        this.idproductprovider = idproductprovider;
    }

    public String getNomproducto() {
        return nomproducto;
    }

    public void setNomproducto(String nomproducto) {
        this.nomproducto = nomproducto;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }


    
}
