/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kallsony.ms.provider.proveedor01.mapper;

import com.kallsony.ms.provider.proveedor01.models.Product;
import com.kallsony.ms.provider.proveedor01.models.externals.Data;
import com.kallsony.ms.provider.proveedor01.models.externals.ResponseListProductsProvider;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jorge
 */
public class MapperListProductsToBusisnessObject {
    final static String NAMEPROVIDER = "Sony";
    public static List<Product> MapOBject(ResponseListProductsProvider rs) {
        List<Product> products = new ArrayList<>();
        rs.getData().stream().map(resultSet -> {
            Product product = new Product();
            //homologar este valor
            product.setSubCategoryId("1");
            product.setProductServiceDescription(resultSet.getName());
            product.setSubCategoryDescription(resultSet.getBrand_name());
            product.setValue(Float.parseFloat(resultSet.getPrice()));
            product.setQuantity(resultSet.getInventory_level());
            product.setNameProvider(NAMEPROVIDER);
            String idProductProvider = resultSet.getCondition() +","+resultSet.getURL();
            product.setiDProductProvider(idProductProvider);
            //product.setValue(resultSet.getValue());
            return product;
        }).forEachOrdered(product -> {
            products.add(product);
        });
        
        return products;
    }


    
}
