/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kallsony.ms.provider.proveedor01.controller;

import com.kallsony.ms.provider.proveedor01.models.RequestCreateOrderKs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import java.net.InetAddress;
import java.net.UnknownHostException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.kallsony.ms.provider.proveedor01.service.ProviderService;

/**
 *
 * @author Jorge
 */
@CrossOrigin
@Controller
public class ProviderController {
    @Autowired
    private ProviderService productService;
    
    @GetMapping(value = "/v1/retrieve/products", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getProducts() throws UnknownHostException {
        System.out.println("Getting all products...");

        final HttpHeaders responseHeaders = new HttpHeaders();

        return ResponseEntity.ok().headers(responseHeaders).body(this.productService.getAllProducts());
    }
    
    @GetMapping(value = "/", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> healthCheck() throws UnknownHostException {
        final String privateIp = InetAddress.getLocalHost().getHostAddress();
        final String privateDns = InetAddress.getLocalHost().getHostName();

        final String jsonResponse = "{" +
                "\"service\":\"ms-provider-proveedor01\"," +
                "\"status\":\"ok\"," +
                "\"privateIp\":\"" + privateIp + "\"," +
                "\"privateDns\":\"" + privateDns + "\"" +
                "}";

        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("dev-essentials-private-ip", privateIp);
        responseHeaders.set("dev-essentials-private-dns", privateDns);

        return ResponseEntity.ok().headers(responseHeaders).body(jsonResponse);
    }
    
    @PostMapping(value = "/v1/retrieve/products",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> postOrder(@RequestBody final RequestCreateOrderKs request) throws UnknownHostException {
        final HttpHeaders responseHeaders = new HttpHeaders();
        System.out.println("Create ORder..."+request);
        return ResponseEntity.ok().headers(responseHeaders).body(this.productService.createOrder(request));  
    }
}
