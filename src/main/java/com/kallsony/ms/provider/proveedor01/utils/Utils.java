/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kallsony.ms.provider.proveedor01.utils;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Jorge
 */
public class Utils {

    public Map<String, Integer> getHomologations() {

        Map<String, Integer> myMap = new HashMap<>();
        //Se puede configurar en BD esto
        String s = "SALES:0,SALE_PRODUCTS:1,EXPENSES:2,EXPENSES_ITEMS:3";
        String[] pairs = s.split(",");
        for (String pair : pairs) {
            String[] keyValue = pair.split(":");
            myMap.put(keyValue[0], Integer.valueOf(keyValue[1]));
        }
        return myMap;
    }

}
