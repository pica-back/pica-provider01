/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kallsony.ms.provider.proveedor01.mapper;

import com.kallsony.ms.provider.proveedor01.models.ResponseCreateOrderKs;
import com.kallsony.ms.provider.proveedor01.models.externals.ResponseCreateOrderProvider;

/**
 *
 * @author Jorge
 */
public class MapperResponseOrderToBusinessObject {
    
    public static ResponseCreateOrderKs MapOBject(ResponseCreateOrderProvider rs) {
        ResponseCreateOrderKs rsKs = new ResponseCreateOrderKs();
        rsKs.setMessage(String.valueOf(rs.getData().getId()));
        rsKs.setStatusCode(0);
        return rsKs;
    }
    
}
